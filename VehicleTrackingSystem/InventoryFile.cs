﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;

namespace VehicleTrackingSystem
{
    class InventoryFile : IEnumerable, IEnumerator
    {
        //private members
        private string[] lines; //lines of the inventory file
        private string fileName;
        private List<Car> currentInventory;
        int position = -1;

        //methods
        public InventoryFile()
        {
            //Get filename
            this.lines = new string[0];
            Console.WriteLine("Enter CSV file name: ");
            this.FileName = Console.ReadLine();
            //see if the user actually put anything in
            if (this.FileName.Length == 0)
            {
                this.FileName = "inventory.csv";
                Console.WriteLine("No file name given. Defaulting to 'inventory.csv'.");
            }
            //try to open and read the file into string array 'lines'
            try
            {
                //Read file if it exists. Otherwise wait until user saves the inventory to create one with the user specified name
                if (File.Exists(this.FileName))
                {
                    //    this.lines = File.ReadAllLines(this.FileName);
                    ReadFileToInventory();
                }
                else
                {
                    Console.WriteLine("File with name " + this.FileName + " does not exist. It will be created when you save the inventory.");
                    File.Create(this.FileName).Close();
                    lines[0] = "Type,Make,Model,Year,Count";
                    File.WriteAllLines(this.FileName, lines);
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n\n" + e.HelpLink);
                return;
            }
            //see if there is actual data to read
            if (this.lines.Length == 0)
            {
                Console.WriteLine("Empty file. No data to read.");
                return;
            }

        }
        /// <summary>
        /// Reads the given file into the program's memory/inventory system.
        /// </summary>
        public void ReadFileToInventory()
        {
            lines = File.ReadAllLines(this.FileName) ;
            if (lines.Length != 0)
            {
                if (lines[0] == "Type,Make,Model,Year,Count")

                {
                    List<Car> invFileContents = new List<Car>();

                    //Get ready for reading CSV
                    var stream = File.OpenRead(this.FileName);
                    var reader = new StreamReader(stream);
                    var csv = new CsvHelper.CsvReader(reader);
                    var record = new Car();
                    //read csv into car array

                    csv.Read();
                    csv.ReadHeader();
                    while(csv.Read())
                    {
                        try {
                            record = csv.GetRecord<Car>();
                            invFileContents.Add(record);
                        }
                        catch (Exception e)
                        { continue; }
                    }

                    this.Inventory = invFileContents;
                    stream.Close();
                }
                else
                {
                    //put headers on and then add the rest of the contents
                    string[] combine = new string[lines.Length + 1];
                    Array.Copy(lines, 0, combine, 1, lines.Length);
                    combine[0] = "Type,Make,Model,Year,Count";
                    this.lines = combine; //save for late
                }
            }
            else
            {
                lines = new string[] { "Type,Make,Model,Year,Count" };
            }
        }

        public void SaveInventoryToFile()
        {
            this.lines = new string[this.Inventory.Count + 1];
            lines[0] = "Type,Make,Model,Year,Count";
            for(int i = 0; i < this.Inventory.Count; i++)
            {
                lines[i + 1] =  this.Inventory[i].Type + "," +
                                this.Inventory[i].Make + "," +
                                this.Inventory[i].Model + "," +
                                this.Inventory[i].Year + "," +
                                this.Inventory[i].Count.ToString();
            }
            File.WriteAllLines(this.FileName, lines);
        }
        //getters/setters
        public List<Car> Inventory
        {
            get { return this.currentInventory; }
            set { this.currentInventory = value; }
        }
        public string FileName
        {
            get { return this.fileName; }
            set { this.fileName = value; }
        }


        //IEnumberable and IEnumerator methods
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }
        public bool MoveNext()
        {
            position++;
            return (position < currentInventory.Count);
        }
        public void Reset()
        {
            position = 0;
        }
        public object Current
        {
            get { return currentInventory[position]; }
        }
    }

    //For mapping CSV to car objects. Uses CsvHelper
    sealed class CarMap : CsvHelper.Configuration.ClassMap<Car>
    {
        public CarMap()
        {
            Map(car => car.Type).Index(0);
            Map(car => car.Make).Index(1);
            Map(car => car.Model).Index(2);
            Map(car => car.Year).Index(3);
            Map(car => car.Count).Index(4).TypeConverter<CsvHelper.TypeConversion.Int32Converter>();
        }
    }

}
