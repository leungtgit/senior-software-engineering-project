﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleTrackingSystem
{
    class Car : IEquatable<Car>
    {
        //Data members needed for car object
        private string type;
        private string make;
        private string model;
        private string year;
        private int count;

        /// <summary>
        /// Constructor for car object takes 4 string descriptors and one integer indicating how many cars to add
        /// </summary>
        /// <param name="Type">Sedan, Truck, Van, etc</param>
        /// <param name="Make">Toyota, Ford, Audi, Mack, Tesla, etc</param>
        /// <param name="Model">Prius, F-150, Soul, etc</param>
        /// <param name="Year">2018</param>
        /// <param name="count">An integer indicating how many cars to add in the case there is a shipment of more than one.</param>
        public Car(string Type, string Make, string Model, string Year, int Count)
        {
            this.type = Type;
            this.make = Make;
            this.model = Model;
            this.year = Year;
            //Have to make sure that the user isn't putting in garbage data like a shipment of -900 2018 Toyota Priuses
            if (Count < 1) {
                this.count = 1;
            } else {
                this.count = Count;
            }

        }
        public Car()
        {
            this.Type = "";
            this.Type = "";
            this.Type = "";
            this.Type = "";
            this.Count = 1;
        }
        /// <summary>
        /// Destructor for car object. Currently there is nothing special that has to be done to delete a car object
        /// </summary>
        ~Car()
        {

        }

        //Member functions
        /// <summary>
        /// Adds a car to the inventory.
        /// </summary>
        /// <param name="Inventory">reference to the inventory list</param>
        public static void AddCar(ref List<Car> Inventory) {
            //Check if null
            if (Inventory == null)
                Inventory = new List<Car>();
            //declare string variables for reading into a new car object
            int index = 0;

            //Prompt user for car details
            Car newCar = PromptCar();

            //Check if car is in the inventory already
            if(newCar.CarExists(Inventory, ref index)) //index and check if car exists already
            {
                Inventory[index].Count += newCar.Count; //Add the new cars to the pre-existing pool of cars of same description
            }
            else
            {
                //Add car to the inventory if it doesn't already exist
                Inventory.Add(newCar);
            }
        }

        /// <summary>
        /// Deletes all of a specific car from the inventory
        /// </summary>
        /// <param name="Inventory"></param>
        public static void DeleteCar(ref List<Car> Inventory)
        {
            Car searchedCar = PromptSearch();
            int index = 0;

            if(searchedCar.CarExists(Inventory, ref index))
            {
                Inventory.RemoveAt(index);
            }
            else
            {
                Console.WriteLine("Can't delete a car that doesn't exist!");
            }

        }
        /// <summary>
        /// Current does no transactions, but it does decrement the inventory count of a specific type of car.
        /// </summary>
        /// <param name="Inventory"></param>
        public static void SellCar(ref List<Car> Inventory)
        {
            int index = 0;
            Car sellCar = PromptSearch();

            if(sellCar.CarExists(Inventory, ref index))
            {
                //Decrement car count if it exists in the inventory and remove the inventory entry from the list if it descends below 1
                Inventory[index].Count--;
                if(Inventory[index].Count <= 0)
                {
                    Inventory.RemoveAt(index);
                }
                //tell user what car was sold
                Console.WriteLine("Sold");
                Console.WriteLine(sellCar.ToString());
            }
            else
            {
                //Tell user they can't sell a car that doesn't exist
                Console.WriteLine("Car does not exist. Cannot be sold")
;            }
        }

        /// <summary>
        /// Returns a list of cars that match the search results provided by the user.
        /// </summary>
        /// <param name="Inventory"></param>
        /// <returns>List<Car> containing cars in the inventory that match the user's search fields</Car></returns>
        public static List<Car> SearchCar(List<Car> Inventory)
        {
            List<Car> searchMatches = new List<Car>();
            //Get search query and make it all lowercase
            Car searchedCar = PromptSearch();
            CarDescToLower(ref searchedCar);

            List<Car> searchResults;

            foreach(Car result in Inventory)
            {
                Car temp = CarDescToLower(result);
                if(SearchMatch(searchedCar, temp))
                {
                    searchMatches.Add(result);
                }
            }
            return searchMatches;
        }

        /// <summary>
        /// Prompts user for car details and returns a car object with said details.
        /// </summary>
        /// <returns></returns>
        public static Car PromptCar() {
            string tempType;
            string tempMake;
            string tempModel;
            string tempYear;
            string sStringCount;
            int tempCount = 0;
            //prompt user for input
            Console.WriteLine("Please enter the following information regarding the vehicle.");
            Console.Write("Type: ");
            tempType = Console.ReadLine();
            Console.Write("Make: ");
            tempMake = Console.ReadLine();
            Console.Write("Model: ");
            tempModel = Console.ReadLine();
            Console.Write("Year: ");
            tempYear = Console.ReadLine();
            Console.Write("Count: ");
            sStringCount = Console.ReadLine();

            //try to parse to int
            if (!int.TryParse(sStringCount, out tempCount))
            {
                Console.WriteLine("Error parsing count. Defaulting to 1");
                tempCount = 1;
            }

            //if parsed lower than or equal to 0 default to 1
            if (tempCount <= 0)
            { tempCount = 1; }
            Car tempCar = new Car(tempType, tempMake, tempModel, tempYear, tempCount);
            return tempCar;
        }

        /// <summary>
        /// Modified PrompCar. Doesn't ask for car count. Only descriptors are needed for search.
        /// </summary>
        /// <returns>Returns a car object with the fields asked the user asked for</returns>
        public static Car PromptSearch()
        {
            string tempType;
            string tempMake;
            string tempModel;
            string tempYear;
            string sStringCount;
            int tempCount = 0;
            //prompt user for input
            Console.WriteLine("Please enter the following information regarding the vehicle.");
            Console.Write("Type: ");
            tempType = Console.ReadLine();
            Console.Write("Make: ");
            tempMake = Console.ReadLine();
            Console.Write("Model: ");
            tempModel = Console.ReadLine();
            Console.Write("Year: ");
            tempYear = Console.ReadLine();

            Car tempCar = new Car(tempType, tempMake, tempModel, tempYear, tempCount);
            return tempCar;
        }
        
        /// <summary>
        /// Gets index of car in inventory that matches search. All fields needed to be accurate. This function is generally used as a helper function.
        /// Call this when adding a new car so it can be properly placed with other cars with the same details.
        /// </summary>
        /// <param name="inventory">Inventory list</param>
        /// <returns></returns>
        public int GetCarIndex(List<Car> inventory) {
            for(int pos = 0; pos < inventory.Count; pos++) {
                if (inventory[pos] == this)
                    return pos;
            }
            return -1;
        }

        /// <summary>
        /// Iterates through the inventory to find a car. Returns true if car found and also sets an index of where to find it in the inventory
        /// </summary>
        /// <param name="inventory"></param>
        /// <param name="carIndex"></param>
        /// <returns></returns>
        public bool CarExists(List<Car> inventory, ref int carIndex) {
            carIndex = this.GetCarIndex(inventory);
            if (carIndex > -1)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Modifies a car's fields to be all lower case.
        /// </summary>
        /// <param name="lowerType"></param>
        /// <param name="lowerMake"></param>
        /// <param name="lowerModel"></param>
        /// <param name="lowerYear"></param>
        public static void CarDescToLower(ref Car lowerCar)
        {
            lowerCar.Type = lowerCar.Type.ToLower();
            lowerCar.Make = lowerCar.Make.ToLower();
            lowerCar.Model = lowerCar.Model.ToLower();
            lowerCar.Year = lowerCar.Year.ToLower();
            return ;

        }
        /// <summary>
        /// Returns a new car object with all fields in lowercase without modifying the original car
        /// </summary>
        /// <param name="lowerCar"></param>
        /// <returns></returns>
        public static Car CarDescToLower(Car lowerCar)
        {
            Car lowerCaseCar = new Car(lowerCar.Type.ToLower(), lowerCar.Make.ToLower(), lowerCar.Model.ToLower(), lowerCar.Year.ToLower(), 1);
            return lowerCaseCar;
        }
        /// <summary>
        /// Rudimentary search function that returns true if the searched details matches a car that comes up as a result from the list.
        /// Not entering anything for a field during a search is a wildcard and will count as true for that particular field.
        /// 
        /// Example:
        /// Searching for car with fields
        /// Type: ""
        /// Make: "Toyota"
        /// Model: ""
        /// Year: "1990"
        /// 
        /// will return true for any 1990 Toyota in the inventory regardless of what the Type and Model fields are.
        /// </summary>
        /// <param name="searchCar">Car object that has the fields you want to search for</param>
        /// <param name="resultCar">Car in the inventory that will be compared to the searchCar</param>
        /// <returns></returns>
        public static bool SearchMatch(Car searchCar, Car resultCar)
        {
            if( (searchCar.Type == resultCar.Type || searchCar.Type.Length == 0) &&
                (searchCar.Make == resultCar.Make || searchCar.Make.Length == 0) &&
                (searchCar.Model == resultCar.Model || searchCar.Model.Length == 0) &&
                (searchCar.Year == resultCar.Year || searchCar.Year.Length == 0) )
            {
                return true;
            }
            else { return false; }

        }
        /// <summary>
        /// Prints all cars in the list<> inventory
        /// </summary>
        /// <param name="Inventory"></param>
        public static void PrintInventory(List<Car> Inventory)
        {
            foreach (Car result in Inventory)
                Console.WriteLine(result.ToString());
        }

        //Getter/setters
        public string Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        public string Make
        {
            get { return this.make; }
            set { this.make = value; }
        }
        public string Model
        {
            get { return this.model; }
            set { this.model = value; }
        }
        public string Year
        {
            get { return this.year; }
            set { this.year = value; }
        }
        public int Count
        {
            get { return this.count; }
            set { this.count = value; }
        }

        //overrides/overloads
        /// <summary>
        /// Returns true if referencing itself.
        /// Returns false if not referencing itself.
        /// Compares descriptions and returns true if descriptions match or false for the opposite
        /// </summary>
        /// <param name="car1">Car that is compared to the second car.</param>
        /// <param name="car2">Car that is compared to the first car</param>
        /// <returns></returns>
        public static bool operator==(Car car1, Car car2)
        {
            //Reference comparisons
            if (ReferenceEquals(car1, car2))
                return true;
            if (ReferenceEquals(car1, null))
                return false;
            if (ReferenceEquals(car2, null))
                return false;

            //Value comparisons
            //Make copy of cars
            Car lowerCar1 = new Car(car1.Type, car1.Make, car1.Model, car1.Year, 0);
            Car lowerCar2 = new Car(car2.Type, car2.Make, car2.Model, car2.Year, 0);
            //make descriptions lowercase
            CarDescToLower(ref lowerCar1);
            CarDescToLower(ref lowerCar2);
            //return result of comparisons
            return      (lowerCar1.Make == lowerCar2.Make) &&
                        (lowerCar1.Type == lowerCar2.Type) &&
                        (lowerCar1.Model == lowerCar2.Model) &&
                        (lowerCar1.Year == lowerCar2.Year);
        }
        public static bool operator!=(Car car1, Car car2)
        {
            return !(car1 == car2);
        }
        public bool Equals(Car obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            return obj.GetType() == GetType() && Equals((Car)obj);
        }
        /// <summary>
        /// Overrides ToString() to properly print the car details as a string on the console
        /// </summary>
        /// <returns>Returns string containing car fields and their values.</returns>
        public override string ToString()
        {
            return "Type:\t" + this.Type +
                    "\nMake:\t" + this.Make +
                    "\nModel:\t" + this.Model +
                    "\nYear:\t" + this.Year +
                    "\nCount:\t" + this.Count + "\n";
        }
    }
}
