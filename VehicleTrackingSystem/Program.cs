﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace VehicleTrackingSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            InventoryFile inv = new InventoryFile();
            List<Car> tempInventory = inv.Inventory;

            int option = 0;
            while (true)
            {
                Console.WriteLine("(1) Add a car batch to the inventory.");
                Console.WriteLine("(2) Delete a car batch from the inventory.");
                Console.WriteLine("(3) Complete a sale.");
                Console.WriteLine("(4) Search for a specific car in the inventory.");
                Console.WriteLine("(5) Print current availability of all vehicles in the inventory.");
                Console.WriteLine("(6) Save current inventory of all vehicles to file.");

                Console.WriteLine("Please enter your selection or enter anything else to exit the program: ");
                Int32.TryParse(Console.ReadLine(), out option);

                switch(option)
                {
                    case 1:
                        Car.AddCar(ref tempInventory);
                        break;
                    case 2:
                        Car.DeleteCar(ref tempInventory);
                        break;
                    case 3:
                        Car.SellCar(ref tempInventory);
                        break;
                    case 4:
                        Car.PrintInventory(Car.SearchCar(tempInventory));
                        break;
                    case 5:
                        Car.PrintInventory(tempInventory);
                        break;
                    case 6:
                        inv.Inventory = tempInventory;
                        inv.SaveInventoryToFile();
                        break;
                    default:
                        Console.WriteLine("\n\nThank you for using this program!\nSee you next time!\n");
                        return;
                }
            }
            return;
        }
    }
}
